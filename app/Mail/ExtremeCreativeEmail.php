<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ExtremeCreativeEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user;
    public $token;

    public function __construct(User $user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('extremecreativebersama@gmail.com')
            ->subject('Verify Your Registration | Extreme Creative')
            ->view('mail')
            ->with(
                [
                    'website' => 'www.extremecreative.id',
                    'from' => 'extremecreativebersama@gmail.com',
                    'to' => $this->user->email,
                    'name' => $this->user->name,
                    'linkVerify' => env('VUE_APP_BASE_URL', 'http://localhost:8080') . '/jwt?token=' . $this->token,
                ]
            );
    }
}
