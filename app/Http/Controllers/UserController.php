<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Mail\ExtremeCreativeEmail;
use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password', 'username');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:30',
            'username' => 'required|alpha_dash|max:20|unique:users',
            'email' => 'required|string|email|max:50|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'dob' => 'required|date'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::create([
            'id' => Uuid::uuid4(),
            'name' => ucwords($request->get('name')),
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'dob' => $request->get('dob'),
        ]);

        $token = JWTAuth::fromUser($user);

        Mail::to($request->get('email'))->send(new ExtremeCreativeEmail($user, $token));

        return response()->json(compact('user', 'token'), 201);
    }

    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], 404);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], 404);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], 404);
        }

        return response()->json(compact('user'));
    }

    public function verifyEmail()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], 404);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], 404);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], 404);
        }

        User::where('id', $user->id)->update(['email_verified_at' => Carbon::now()]);

        return response()->json(compact('user'));
    }

    public function get(Request $request)
    {
        if ($request->input('id') || $request->input('username')) {

            $user = $request->input('id') ? User::find($request->input('id')) : User::where('username', $request->input('username'))->first();

            if ($user) {
                return response()->json(compact('user'), 201);
            }
            return response()->json(['message' => 'User Not Found'], 404);
        }

        $search     = $request->input('search') ? $request->input('search') : '';
        $limit      = $request->input('limit') ? $request->input('limit') : 9999999;
        $orderBy    = $request->input('orderBy') ? $request->input('orderBy') : 'created_at';
        $sort       = $request->input('sort') ? $request->input('sort') : 'desc';

        $users = User::where(function ($query) use ($search) {
            $query->where('username', 'like', '%' . $search . '%')
                ->orWhere('name', 'like', '%' . $search . '%');
        })->orderBy($orderBy, $sort)->paginate($limit);

        return response()->json(compact('users'), 201);
    }
}
