<?php

use App\User;
use Ramsey\Uuid\Uuid;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $faker = \Faker\Factory::create();
        $password = Hash::make('123123A');

        User::create([
            'id' => Uuid::uuid4(),
            'name' => 'Superadmin',
            'email' => 'superadmin@extremecreative.id',
            'username' => 'superadmin',
            'password' => $password,
            'dob' => $faker->dateTime($max = 'now', $timezone = null)
        ]);

        for ($i = 0; $i < 100; $i++) {
            User::create([
                'id' => Uuid::uuid4(),
                'name' => $faker->name,
                'email' => $faker->email,
                'username' => $faker->userName,
                'password' => $password,
                'dob' => $faker->dateTime($max = 'now', $timezone = null)
            ]);
        }
    }
}
